package net.clockworkcode.math.calc;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

import static net.clockworkcode.math.calc.Type.COMMA;
import static net.clockworkcode.math.calc.Type.FUNCTION;
import static net.clockworkcode.math.calc.Type.FUNCTION_LEFT_PAREN;
import static net.clockworkcode.math.calc.Type.FUNCTION_RIGHT_PAREN;
import static net.clockworkcode.math.calc.Type.LEFT_PAREN;
import static net.clockworkcode.math.calc.Type.NONE;
import static net.clockworkcode.math.calc.Type.NUMBER;
import static net.clockworkcode.math.calc.Type.OPERATOR;
import static net.clockworkcode.math.calc.Type.RIGHT_PAREN;
import static net.clockworkcode.math.calc.Type.VARIABLE;

/**
 * Created by david on 12/04/2016.
 */
public class InfixTokeniser implements Iterator<Token> {
    private Set<String> functionNames;
    private Set<String> variableNames;
    private List<String> input;
    private Stack<Type> parenStack = new Stack<>();
    private Token previousToken;
    private int index = 0;

    public InfixTokeniser(String input, Set<String> functionNames, Set<String> variableNames) {
        this.input = split(input);
        this.functionNames = functionNames;
        this.variableNames = variableNames;
    }

    public InfixTokeniser(String input) {
        this.input = split(input);
        this.functionNames = Collections.EMPTY_SET;

    }

    @Override
    public boolean hasNext() {
        return index < input.size();
    }

    @Override
    public Token next() {
        if (!hasNext()) {
            throw new NoSuchElementException("No more elements, index: " + index);
        }
        String currentValue = input.get(index);
        Token token = createToken(currentValue);
        index++;
        previousToken = token;
        return token;
    }

    private Token createToken(String currentValue) {
        Optional<? extends Token> token = maybeParen(currentValue);
        if (!token.isPresent()) {
            token = maybeComma(currentValue);
        }
        if (!token.isPresent()) {
            token = maybeFunction(currentValue);
        }
        if (!token.isPresent()) {
            token = maybeVariable(currentValue);
        }
        if (!token.isPresent()) {
            token = maybeDashOperator(currentValue);
        }
        if (!token.isPresent()) {
            token = maybeOperator(currentValue);
        }
        if (!token.isPresent()) {
            token = maybeNumeric(currentValue);
        }
        if (!token.isPresent()) {
            throw new RuntimeException(String.format("Could not determine token type for %1$s at index %2$d", currentValue, index));
        }
        return token.get();
    }

    private Optional<? extends Token> maybeParen(String currentValue) {
        Type type = NONE;
        if(currentValue.equals("(")) {
            type = previousTokenisFunction() ? FUNCTION_LEFT_PAREN : LEFT_PAREN;
            parenStack.push(type);
        } else if(currentValue.equals(")")) {
            Type openingParamWas = parenStack.pop();
            type = (openingParamWas == FUNCTION_LEFT_PAREN) ? FUNCTION_RIGHT_PAREN : RIGHT_PAREN;
        }
        return type == NONE ? Optional.empty() : Optional.of(new Token(currentValue, type));
    }

    private boolean previousTokenisFunction() {
        return previousToken != null && previousToken.getType() == FUNCTION;
    }

    private Optional<NumericToken> maybeNumeric(String currentValue) {
        BigDecimal number = null;
        if (Character.isDigit(currentValue.charAt(0))) {
            try {
                number = new BigDecimal(currentValue);
            } catch (Exception e) {
                throw new RuntimeException("Could not convert numeric value " + currentValue);
            }
        }
        return number == null ? Optional.empty() : Optional.of(new NumericToken(currentValue, NUMBER, number));
    }

    private Optional<Token> maybeFunction(String currentValue) {
        return functionNames.contains(currentValue) ? Optional.of(new FunctionNameToken(currentValue, FUNCTION)) : Optional.empty();
    }
    private Optional<Token> maybeVariable(String currentValue) {
        return variableNames.contains(currentValue) ? Optional.of(new FunctionNameToken(currentValue, VARIABLE)) : Optional.empty();
    }

    private Optional<Token> maybeComma(String currentValue) {
        return currentValue.equals(",") ? Optional.of(new Token(",", COMMA)) : Optional.empty();
    }

    private Optional<OperatorToken> maybeDashOperator(String currentValue) {
        if (currentValue.equals("-")) {
            Operator operator = Operator.UNARY_MINUS;
            // TODO handle minus and unary minus
            if (previousToken != null && (
                    previousToken.getType() == FUNCTION_RIGHT_PAREN ||
                    previousToken.getType() == RIGHT_PAREN ||
                    previousToken.getType() == VARIABLE ||
                    previousToken.getType() == NUMBER
                    )) {
                operator = Operator.MINUS;
            }
            // unary minus
            // index == 0  - start of expression - 3 + 4
            // previous token is an arithmetic operator 3 * -8
            // previous token is a left paren/function left paren (-4 + 4) + sin(-90)

            // subtract
            // previous token is a number     9 - 8
            // previous token is a variable   x - 8
            // previous token is a function   sin(x) - 8
            return Optional.of(new OperatorToken(currentValue, OPERATOR, operator));
        } else {
            return Optional.empty();
        }
    }

    private Optional<OperatorToken> maybeOperator(String currentValue) {
        Optional<Operator> operator = Operator.valueOfSymbol(currentValue);
        if (operator.isPresent()) {
            return Optional.of(new OperatorToken(currentValue, OPERATOR, operator.get()));
        } else {
            return Optional.empty();
        }
    }

    /*
     From http://stackoverflow.com/a/21408785/5270540:

     It will split the string at any place that is either preceded or followed by a non-alphanumeric character or period.

     (?<=[^\.a-zA-Z\d]) is a positive lookbehind. It matches the place between two characters,
     if the preceding string matches the sub-regex contained within (?<=...).
     [^\.a-zA-Z\d] is a negated character class. It matches a single character that is not contained within [^...].
     \. matches the character ..
     a-z matches any lowercase character between a and z.
     A-Z is the same, but for uppercase.
     \d is the equivalent of [0-9], so it matches any digit.
     | is the equivalent of an "or". It makes the regex match either the preceding half of the regex or the following half.
     (?=[^\.a-zA-Z\d]) is the same as the first half of the regex, except that it is a positive lookahead. It matches the place between two characters,
     if the following string matches the sub-regex contained within (?=...).
     */
    private static List<String> split(String input) {
        // TODO pre-process to convert engineering formats such as 2.3e-4 or 2.3E4 but don't confuse with logarithmic e?
        // or do in token stream
        return Arrays.stream(input.split("(?<=[^\\.a-zA-Z\\d])|(?=[^\\.a-zA-Z\\d])"))
                .map(String::trim)
                .filter(t -> t.length() > 0)
                .collect(Collectors.toList());
    }
}
