package net.clockworkcode.math.calc;

import java.math.BigDecimal;

/**
 * Created by david on 17/04/2016.
 */
public class Variable {
    private String name;
    private BigDecimal value;

    public Variable(String name, BigDecimal value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getValue() {
        return value;
    }
}
