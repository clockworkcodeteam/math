package net.clockworkcode.math.calc;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by david on 17/04/2016.
 */
public class Variables {
    Map<String, Variable> variables = new HashMap<>();
    public void add(Variable variable) {
        variables.put(variable.getName(), variable);
    }
    public Set<String> getVariableNames() {
        return variables.keySet();
    }
    public BigDecimal getValue(String name) {
        Variable variable = variables.get(name);
        if(variable == null) {
            throw new RuntimeException("No variable exists with name " + name);
        }
        return variable.getValue();
    }
}
