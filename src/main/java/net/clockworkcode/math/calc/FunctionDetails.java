package net.clockworkcode.math.calc;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;

/**
 * Created by david on 16/04/2016.
 */
public class FunctionDetails {
    private String name;
    private Function<List<BigDecimal>, BigDecimal> function;
    private Integer arity;
    private boolean variadic;

    public boolean isVariadic() {
        return variadic;
    }

    public Integer getArity() {
        return arity;
    }

    public String getName() {
        return name;
    }

    public FunctionDetails(String name, Function<List<BigDecimal>, BigDecimal> function, Integer arity) {
        this.name = name;
        this.function = function;
        this.arity = arity;
        this.variadic = false;
    }

    public FunctionDetails(String name, Function<List<BigDecimal>, BigDecimal> function) {
        this.name = name;
        this.function = function;
        this.variadic = true;
    }

    public BigDecimal execute(List<BigDecimal> args) {
        if(!variadic && args.size() != arity) {
            throw new RuntimeException("Number of arguments passed does not match the arity of " + name + " arity: " + arity);
        }
        return function.apply(args);
    }

}
