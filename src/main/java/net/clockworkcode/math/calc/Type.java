package net.clockworkcode.math.calc;

/**
 * Created by david on 12/04/2016.
 */
public enum Type {
    FUNCTION,
    FUNCTION_LEFT_PAREN,
    FUNCTION_RIGHT_PAREN,
    NUMBER,
    LEFT_PAREN,
    RIGHT_PAREN,
    COMMA,
    OPERATOR,
    VARIABLE,
    NONE
}
