package net.clockworkcode.math.calc;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by david on 8/04/2016.
 */
public enum Operator {
    PLUS("+", 0, true, 2, new FunctionDetails("add", l -> l.get(0).add(l.get(1)))),
    MINUS("-", 0, true, 2, new FunctionDetails("subtract", l -> l.get(0).subtract(l.get(1)))),
    MULTIPLY("*", 1, true, 2, new FunctionDetails("multiply", l -> l.get(0).multiply(l.get(1)))),
    DIVIDE("/", 1, true, 2, new FunctionDetails("divide", l -> l.get(0).divide(l.get(1), 15, BigDecimal.ROUND_HALF_UP))),
    REMAINDER("%", 1, true, 2, new FunctionDetails("remainder", l -> l.get(0).remainder(l.get(1)))),
    UNARY_MINUS("-", 2, true, 1, new FunctionDetails("unary_minus", l -> l.get(0).negate())),
    POWER("^", 3, false, 2, new FunctionDetails("power", l -> {
            if(l.get(1).stripTrailingZeros().precision() > 1) {
                throw new RuntimeException(" Second argument of power needs to be an integer, was " + l.get(1));
            }
            return l.get(0).pow(l.get(1).intValue());
        }
    ));
    Map<String, Operator> operator = new HashMap<>();

    String symbol;
    int arguments;
    int precedence;

    boolean leftAssociative;

    FunctionDetails calc;
    Operator(String symbol, int precedence, boolean leftAssociative, int arguments, FunctionDetails calc) {
        this.symbol = symbol;
        this.precedence = precedence;
        this.leftAssociative = leftAssociative;
        this.arguments = arguments;
        this.calc = calc;
    }

    public static Optional<Operator> valueOfSymbol(String symbol) {
        if(symbol == null) {
            return Optional.empty();
        }
        if(symbol.equals("-")) {
            throw new RuntimeException("Symbol for subtract and unary minus are the same and cannot be determined via valueOfSymbol");
        }
        return Arrays.stream(Operator.values())
                .filter(o -> o.getSymbol().equals(symbol))
                .findFirst();
    }

    public int getArguments() {
        return arguments;
    }

    public FunctionDetails getCalc() {
        return calc;
    }

    public boolean isLeftAssociative() {
        return leftAssociative;
    }

    public String getSymbol() {
        return symbol;
    }

    public int getPrecedence() {
        return precedence;
    }

    public static String symbols() {
        return Arrays.stream(Operator.values())
                .map(Operator::getSymbol)
                .collect(Collectors.joining());
    }
}
