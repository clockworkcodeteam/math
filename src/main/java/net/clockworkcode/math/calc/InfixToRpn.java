package net.clockworkcode.math.calc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static net.clockworkcode.math.calc.Type.FUNCTION_LEFT_PAREN;
import static net.clockworkcode.math.calc.Type.LEFT_PAREN;

/**
 * Created by david on 6/04/2016.
 * see http://wcipeg.com/wiki/Shunting_yard_algorithm
 * see https://en.wikipedia.org/wiki/Shunting-yard_algorithm
 */
public class InfixToRpn {
    private Set<String> functionNames;
    private Set<String> variableNames;
    private static Logger LOG = LoggerFactory.getLogger(InfixToRpn.class);
    private InfixToRpnState state = new InfixToRpnState();
    public InfixToRpn() {
        functionNames = Collections.emptySet();
        variableNames = Collections.emptySet();
    }

    public InfixToRpn(Set<String> functionNames, Set<String> variableNames) {
        this.functionNames = functionNames;
        this.variableNames = variableNames;
    }

    public List<Token> convert(String input) {
//        state = new InfixToRpnState(); // TODO test two calcs in a row
        LOG.info("converting " + input);
        LOG.debug("| Token | Action | Output | Stack | Notes |");
        // wrapping paren makes algorithm simpler, no need to pop stack at end
        InfixTokeniser tokeniser = new InfixTokeniser("(" + input + ")", functionNames, variableNames);
        while (tokeniser.hasNext()) {
            state.setCurrentToken(tokeniser.next());
            switch (state.getCurrentToken().getType()) {
                case NUMBER:
                case VARIABLE:
                    state.addCurrentTokenToResult();
                    break;
                case OPERATOR:
                    handleOperator();
                    break;
                case LEFT_PAREN:
                case FUNCTION_LEFT_PAREN:
                    state.addCurrentTokenToStack();
                    break;
                case FUNCTION:
                    handleFunction();
                    break;
                case RIGHT_PAREN:
                    handleRightParen();
                    break;
                case COMMA:
                    handleComma();
                    break;
                case FUNCTION_RIGHT_PAREN:
                    handleFunctionRightParen();

                    break;
                default:
                    throw new RuntimeException("Unknown token " + state.getCurrentToken().getValue());
            }
        }

        return state.getResult();
    }

    private void handleFunction() {
        state.addCurrentTokenToStack();
        state.newFunction();
    }

    private void handleComma() {
        popToResultUntilLeftParenFound(FUNCTION_LEFT_PAREN);
        state.addFunctionParam();
    }

    private void handleFunctionRightParen() {
        popToResultUntilLeftParenFound(FUNCTION_LEFT_PAREN);
        // discard the left paren
        state.popStack();
        setFunctionArity();
        // output function name to the result
        state.popStackToResult();
    }

    private void setFunctionArity() {
        FunctionNameToken token = (FunctionNameToken)state.peekStack().get();
        token.setArity(state.getFunctionParamCount());
    }

    private void handleRightParen() {
        popToResultUntilLeftParenFound(LEFT_PAREN);
        // discard the left paren
        state.popStack();
    }

    private void popToResultUntilLeftParenFound(Type parenType) {
        Optional<Token> op = null;
        while ((op = state.peekStack()).isPresent() && op.get().getType() != parenType) {
            state.popStackToResult();
        }
    }

    private void handleOperator() {
        if (state.stackIsEmpty()) {
            state.addCurrentTokenToResult();
        } else {
            OperatorToken tokenOp = (OperatorToken) state.getCurrentToken();
            Optional<Token> tokenStack = null;
            while ((tokenStack = state.peekStack()).isPresent() && tokenStack.get() instanceof OperatorToken) {
                if(tokenOp.lowerPreferenceThan((OperatorToken) tokenStack.get())) {
                    state.popStackToResult();
                } else {
                    break;
                }
            }
            state.addCurrentTokenToStack();
        }
    }
}
