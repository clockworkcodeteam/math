package net.clockworkcode.math.calc;

/**
 * Created by david on 15/04/2016.
 */
public class FunctionNameToken extends Token {
    int arity = 0;

    public int getArity() {
        return arity;
    }

    public void setArity(int arity) {
        this.arity = arity;
    }

    public FunctionNameToken(String value, Type type) {
        super(value, type);
    }
}
