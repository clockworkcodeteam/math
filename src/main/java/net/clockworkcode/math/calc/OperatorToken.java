package net.clockworkcode.math.calc;

/**
 * Created by david on 12/04/2016.
 */
public class OperatorToken extends Token {
    private Operator operator;

    public Operator getOperator() {
        return operator;
    }

    public OperatorToken(String value, Type type, Operator operator) {
        super(value, type);
        this.operator = operator;
    }
    public boolean lowerPreferenceThan(OperatorToken other) {
        return operator.isLeftAssociative() && operator.getPrecedence() <= other.operator.getPrecedence() ||
                !operator.isLeftAssociative() && operator.getPrecedence() < other.operator.getPrecedence();
    }
}
