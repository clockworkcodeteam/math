package net.clockworkcode.math.calc;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by david on 12/04/2016.
 */
public class Token {
    private String value;
    private Type type;

    public Token(String value, Type type) {
        this.value = value;
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public static String joinValues(List<Token> tokens, String delimiter) {
        return join(tokens, delimiter, Token::getValue);
    }
    public static String join(List<Token> tokens, String delimiter, Function<Token, String> tokenToString) {
        return tokens.stream()
                .map(tokenToString)
                .collect(Collectors.joining(delimiter));
    }

}
