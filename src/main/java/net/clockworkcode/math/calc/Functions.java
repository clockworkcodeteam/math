package net.clockworkcode.math.calc;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by david on 16/04/2016.
 */
public class Functions {
    private Map<String, FunctionDetails> functionMap = new HashMap<>();

    public BigDecimal execute(String name, List<BigDecimal> args) {
        return get(name).execute(args);
    }

    public void add(FunctionDetails function) {
        functionMap.put(function.getName(), function);
    }
    public Set<String> getFunctionNames() {
        return functionMap.keySet();
    }
    public FunctionDetails get(String name) throws RuntimeException {
        FunctionDetails functionDetails = functionMap.get(name);
        if(functionDetails == null) {
            throw new RuntimeException(String.format("Function {} is not supported", name));
        }
        return functionDetails;
    }
}
