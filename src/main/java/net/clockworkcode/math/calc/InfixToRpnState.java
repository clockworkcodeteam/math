package net.clockworkcode.math.calc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Stack;

/**
 * Created by david on 15/04/2016.
 */
public class InfixToRpnState {
    private static Logger LOG = LoggerFactory.getLogger(InfixToRpnState.class);

    private List<Token> result = new ArrayList<>();

    private Stack<Token> opStack = new Stack<>();
    private Stack<Integer> functionParamCount = new Stack<>();
    private Token currentToken = null;

    public void newFunction() {
        functionParamCount.push(1);
    }
    public void addFunctionParam() {
        Integer currentCount = functionParamCount.pop();
        functionParamCount.push(currentCount + 1);
    }
    public int getFunctionParamCount() {
        return functionParamCount.pop();
    }
    public List<Token> getResult() {
        return result;
    }

    public Token getCurrentToken() {
        return currentToken;
    }

    public void setCurrentToken(Token currentToken) {
        this.currentToken = currentToken;
    }

    public Optional<Token> peekStack() {
        return opStack.isEmpty() ? Optional.empty() : Optional.of(opStack.peek());
    }
    public boolean stackIsEmpty() {
        return opStack.isEmpty();
    }
    public Token popStack() {
        Token pop = opStack.pop();
        log("Pop stack", "");
        return pop;
    }
    public Token popStackToResult() {
        Token pop = opStack.pop();
        result.add(pop);
        String notes = ((pop instanceof FunctionNameToken) ? ("Parameter count: " + ((FunctionNameToken) pop).arity) : "");
        log("Pop to output " + pop.getValue(), notes);
        return pop;
    }
    public void addCurrentTokenToResult() {
        result.add(currentToken);
        log("Add to output", "");
    }
    public void addCurrentTokenToStack() {
        opStack.add(currentToken);
        log("Add to stack", "");
    }

    private void log(String action, String notes) {
        LOG.debug("| {} | {} | {} | {} | {} |", currentToken == null ? "end" : currentToken.getValue(), action, Token.joinValues(result, " "), Token.joinValues(opStack, ""), notes);
    }
}
