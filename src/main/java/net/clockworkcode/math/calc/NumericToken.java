package net.clockworkcode.math.calc;

import java.math.BigDecimal;

/**
 * Created by david on 12/04/2016.
 */
public class NumericToken extends Token {
    private BigDecimal number;

    public BigDecimal getNumber() {
        return number;
    }

    public NumericToken(String value, Type type, BigDecimal number) {
        super(value, type);
        this.number = number;
    }
}
