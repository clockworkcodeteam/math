package net.clockworkcode.math.calc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

/**
 * Created by david on 10/04/2016.
 * see https://en.wikipedia.org/wiki/Reverse_Polish_notation
 */
public class RpnCalculator {
    private static Logger LOG = LoggerFactory.getLogger(RpnCalculator.class);
    private Functions functions = new Functions();
    private Variables variables = new Variables();
    private Stack<BigDecimal> resultStack = new Stack<>();
    private Token currentToken;

    public BigDecimal calculate(List<Token> tokens) {
        resultStack = new Stack<>();
        LOG.info("Calculating {}", Token.joinValues(tokens, " "));
        LOG.debug("| Token | Action | Stack | Notes |");

        for (Token token : tokens) {
            currentToken = token;
            switch (currentToken.getType()) {
                case NUMBER:
                    resultStack.push(((NumericToken) currentToken).getNumber());
                    log(currentToken, "Push value", resultStack, "");
                    break;
                case VARIABLE:
                    resultStack.push(variables.getValue(token.getValue()));
                    log(currentToken, "Push value", resultStack, "");
                    break;
                case OPERATOR:
                    Operator op = ((OperatorToken) currentToken).getOperator();
                    execute(op.getArguments(), op.getCalc());
                    break;
                case FUNCTION:
                    FunctionNameToken funtionToken = (FunctionNameToken) currentToken;
                    execute(funtionToken.getArity(), functions.get(funtionToken.getValue()));
                    break;
            }
        }
        if (resultStack.size() > 1) {
            throw new RuntimeException("Too many values after calculation, stack " + resultStack);
        }
        return resultStack.pop();
    }

    private void execute(int arity, FunctionDetails function) {
        List<BigDecimal> args = popRequiredArgs(arity, function);
        log(currentToken, "Pop Function Args", resultStack, args);
        BigDecimal result = function.execute(args);
        resultStack.push(result);
        log(currentToken, "Push value", resultStack, result);
    }

    private List<BigDecimal> popRequiredArgs(int arity, FunctionDetails function) {
        if (function.isVariadic() && arity > resultStack.size()) {
            throw new RuntimeException("not enough operands for the calculation, op " + function.getName() + " stack " + resultStack);
        }
        List<BigDecimal> funcArgs = new ArrayList<>();
        for (int i = 0; i < arity; i++) {
            funcArgs.add(resultStack.pop());
        }
        // correct order resulting from pushing then popping from stack
        Collections.reverse(funcArgs);
        return funcArgs;
    }

    public void add(FunctionDetails function) {
        functions.add(function);
    }
    public void add(Variable variable) {
        variables.add(variable);
    }

    public Set<String> getFunctionNames() {
        return functions.getFunctionNames();
    }

    public Set<String> getVariableNames() {
        return variables.getVariableNames();
    }

    private static void log(Token token, String action, Stack<BigDecimal> stack, List<BigDecimal> operands) {
        String operandsJoined = operands.stream()
                .map(BigDecimal::toString)
                .collect(Collectors.joining(", "));
        log(token, action, stack, String.format("Args: %1$s", operandsJoined));
    }

    private static void log(Token token, String action, Stack<BigDecimal> stack, BigDecimal result) {
        log(token, action, stack, String.format("Result: %1$f", result));
    }

    private static void log(Token token, String action, Stack<BigDecimal> stack, String notes) {
        String stackValues = stack.stream()
                .map(BigDecimal::toString)
                .collect(Collectors.joining(" "));
        LOG.debug("| {} | {} | {} | {} |", token.getValue(), action, stackValues, notes);
    }

}
