package net.clockworkcode.math.calc;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Created by david on 8/04/2016.
 */
public class OperatorTest {
    @Test
    public void testCreateFromSymbol() {
        check(Operator.MULTIPLY, "*");
        check(Operator.DIVIDE, "/");
        check(Operator.PLUS, "+");
        check(Operator.REMAINDER, "%");
        check(Operator.POWER, "^");
    }
    @Test
    public void testCreateFromSymbolFailsUnknownSymbol() {
        assertFalse(Operator.valueOfSymbol("junk").isPresent());
    }
    @Test(expected = RuntimeException.class)
    public void testCreateFromSymbolFailsDash() {
        assertFalse(Operator.valueOfSymbol("-").isPresent());
    }
    @Test
    public void testCreateFromSymbolFailsBlankSymbol() {
        assertFalse(Operator.valueOfSymbol("").isPresent());
    }
    @Test
    public void testCreateFromSymbolFailsNullSymbol() {
        assertFalse(Operator.valueOfSymbol(null).isPresent());
    }

    @Test
    public void calculatePlus() {
        BigDecimal result = Operator.PLUS.getCalc().execute(Arrays.asList(BigDecimal.valueOf(5.5), BigDecimal.valueOf(1.5)));
        assertEquals(BigDecimal.valueOf(7.0), result);
    }
    @Test
    public void calculateMinus() {
        BigDecimal result = Operator.MINUS.getCalc().execute(Arrays.asList(BigDecimal.valueOf(5.5), BigDecimal.valueOf(1.5)));
        assertEquals(BigDecimal.valueOf(4.0), result);
    }
    @Test
    public void calculateMultiply() {
        BigDecimal result = Operator.MULTIPLY.getCalc().execute(Arrays.asList(BigDecimal.valueOf(5.5), BigDecimal.valueOf(1.5)));
        assertEquals(BigDecimal.valueOf(8.25), result);
    }
    @Test
    public void calculateRemainder() {
        BigDecimal result = Operator.REMAINDER.getCalc().execute(Arrays.asList(BigDecimal.valueOf(8), BigDecimal.valueOf(5)));
        assertEquals(BigDecimal.valueOf(3), result);
    }
    @Test
    public void calculateDivide() {
        BigDecimal result = Operator.DIVIDE.getCalc().execute(Arrays.asList(BigDecimal.valueOf(5.5), BigDecimal.valueOf(1.5)));
        assertEquals(BigDecimal.valueOf(3.666666666666667), result);
    }
    @Test
    public void calculatePower() {
        BigDecimal result = Operator.POWER.getCalc().execute(Arrays.asList(BigDecimal.valueOf(3), BigDecimal.valueOf(2.0)));
        assertEquals(BigDecimal.valueOf(9), result);
    }
    @Test(expected = RuntimeException.class)
    public void calculatePowerErrorIfFractionalSecondArg() {
        BigDecimal result = Operator.POWER.getCalc().execute(Arrays.asList(BigDecimal.valueOf(5.5), BigDecimal.valueOf(1.5)));
    }
    private void check(Operator enumValue, String symbol) {
        assertEquals(enumValue, Operator.valueOfSymbol(symbol).get());
    }
}