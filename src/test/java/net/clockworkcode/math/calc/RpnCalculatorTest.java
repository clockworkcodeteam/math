package net.clockworkcode.math.calc;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

/**
 * Created by david on 10/04/2016.
 */
public class RpnCalculatorTest {
    @Rule
    public ExpectedException exc = ExpectedException.none();

    @Test
    public void calculateParen()  {
        BigDecimal result = new RpnCalculator().calculate(new InfixToRpn().convert("( 3*4 ) / ( 0.5 + 0.3 + 1.2 )"));
        assertEquals(BigDecimal.valueOf(6), result.setScale(0));
    }
    @Test
    public void calculateUnaryMinus()  {
        BigDecimal result = new RpnCalculator().calculate(new InfixToRpn().convert("2 + -3.5"));
        assertEquals(BigDecimal.valueOf(-1.5), result);
    }
    @Test
    public void calculatePower()  {
        BigDecimal result = new RpnCalculator().calculate(new InfixToRpn().convert("3^2"));
        assertEquals(BigDecimal.valueOf(9), result);
    }
    @Test
    public void calculateRemainder()  {
        BigDecimal result = new RpnCalculator().calculate(new InfixToRpn().convert("8%5"));
        assertEquals(BigDecimal.valueOf(3), result);
    }
    @Test
    public void calculateComplex()  {
        BigDecimal result = new RpnCalculator().calculate(new InfixToRpn().convert("3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3"));
        assertEquals(BigDecimal.valueOf(3.000122), result.setScale(6, BigDecimal.ROUND_HALF_UP));
    }
    @Test
    public void calculateVariadicFunctions()  {
        RpnCalculator rpnCalculator = new RpnCalculator();
        rpnCalculator.add(new FunctionDetails("max", args -> args.stream().max(BigDecimal::compareTo).get()));
        BigDecimal result = rpnCalculator.calculate(new InfixToRpn(rpnCalculator.getFunctionNames(), Collections.emptySet()).convert("max(1,2,3,max(4,5))"));
        assertEquals(BigDecimal.valueOf(5), result);
    }
    @Test
    public void calculateEngineeringFormat()  {
        BigDecimal result = new RpnCalculator().calculate(new InfixToRpn().convert("1e4/1E3"));
        assertEquals(BigDecimal.valueOf(10), result.setScale(0));
    }
    @Test
    public void calculateEngineeringFormatWitheVariable()  {
        RpnCalculator rpnCalculator = new RpnCalculator();
        rpnCalculator.add(new Variable("e", new BigDecimal("2.71828")));
        BigDecimal result = rpnCalculator.calculate(new InfixToRpn(rpnCalculator.getFunctionNames(), rpnCalculator.getVariableNames()).convert("0.271828e1/e"));
        assertEquals(BigDecimal.valueOf(1), result.setScale(0));
    }
    @Test
    public void calculateVariablesSimple()  {
        RpnCalculator rpnCalculator = new RpnCalculator();
        rpnCalculator.add(new Variable("x", BigDecimal.TEN));
        BigDecimal result = rpnCalculator.calculate(new InfixToRpn(rpnCalculator.getFunctionNames(), rpnCalculator.getVariableNames()).convert("x"));
        assertEquals(BigDecimal.TEN, result);
    }
    @Test
    public void calculateVariablesComplex()  {
        RpnCalculator rpnCalculator = new RpnCalculator();
        rpnCalculator.add(new Variable("cost", new BigDecimal("25")));
        rpnCalculator.add(new Variable("sell", new BigDecimal("50")));
        BigDecimal result = rpnCalculator.calculate(new InfixToRpn(rpnCalculator.getFunctionNames(), rpnCalculator.getVariableNames()).convert("(sell-cost)/cost*100"));
        assertEquals(100, result.intValue());
    }
    @Test
    public void calculateJustANumber() {
        BigDecimal result = new RpnCalculator().calculate(new InfixToRpn().convert("3.5"));
        assertEquals(BigDecimal.valueOf(3.5), result);
    }
    @Test
    public void notEnoughOperandsError() throws Exception {
        exc.expect(RuntimeException.class);
        exc.expectMessage("not enough operands for the calculation, op multiply stack [1]");
        new RpnCalculator().calculate(new InfixToRpn().convert("1 *"));
    }
    @Test
    public void unknownOperatorError() throws Exception {
        exc.expect(RuntimeException.class);
        exc.expectMessage("Could not determine token type for & at index 3");
        new RpnCalculator().calculate(new InfixToRpn().convert("1 2 &"));
    }
    @Test
    public void tooManyItemsOnStackAtEndOfCalc() throws Exception {
        exc.expect(RuntimeException.class);
        exc.expectMessage("Too many values after calculation, stack [1, 2]");
        new RpnCalculator().calculate(new InfixToRpn().convert("1 2"));
    }


}