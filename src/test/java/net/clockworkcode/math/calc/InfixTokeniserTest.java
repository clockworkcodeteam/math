package net.clockworkcode.math.calc;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by david on 12/04/2016.
 */
public class InfixTokeniserTest {
    @Test
    public void tokeniseComplex() throws Exception {
        Set<String> functionNames = new HashSet<>(Arrays.asList("max"));
        Set<String> variableNames = new HashSet<>(Arrays.asList("x"));
        InfixTokeniser tokeniser = new InfixTokeniser("-3 + max(1,4) * 0.2 / ( x ^ 3 ) - 6 % 8", functionNames, variableNames);
        checkNext(tokeniser, "-", Type.OPERATOR, Operator.UNARY_MINUS);
        checkNext(tokeniser, "3", Type.NUMBER);
        checkNext(tokeniser, "+", Type.OPERATOR, Operator.PLUS);
        checkNext(tokeniser, "max", Type.FUNCTION);
        checkNext(tokeniser, "(", Type.FUNCTION_LEFT_PAREN);
        checkNext(tokeniser, "1", Type.NUMBER, BigDecimal.ONE);
        checkNext(tokeniser, ",", Type.COMMA);
        checkNext(tokeniser, "4", Type.NUMBER, new BigDecimal(4));
        checkNext(tokeniser, ")", Type.FUNCTION_RIGHT_PAREN);
        checkNext(tokeniser, "*", Type.OPERATOR, Operator.MULTIPLY);
        checkNext(tokeniser, "0.2", Type.NUMBER, new BigDecimal("0.2"));
        checkNext(tokeniser, "/", Type.OPERATOR, Operator.DIVIDE);
        checkNext(tokeniser, "(", Type.LEFT_PAREN);
        checkNext(tokeniser, "x", Type.VARIABLE);
        checkNext(tokeniser, "^", Type.OPERATOR, Operator.POWER);
        checkNext(tokeniser, "3", Type.NUMBER, new BigDecimal(3));
        checkNext(tokeniser, ")", Type.RIGHT_PAREN);
        checkNext(tokeniser, "-", Type.OPERATOR, Operator.MINUS);
        checkNext(tokeniser, "6", Type.NUMBER, new BigDecimal(6));
        checkNext(tokeniser, "%", Type.OPERATOR, Operator.REMAINDER);
        checkNext(tokeniser, "8", Type.NUMBER, new BigDecimal(8));
    }
    @Test
    public void tokeniseDash() throws Exception {
        Set<String> functionNames = new HashSet<>(Arrays.asList("max"));
        InfixTokeniser tokeniser = new InfixTokeniser("-3 - max(-1-3,-4) - -2", functionNames, Collections.emptySet());
        checkNext(tokeniser, "-", Type.OPERATOR, Operator.UNARY_MINUS);
        checkNext(tokeniser, "3", Type.NUMBER);
        checkNext(tokeniser, "-", Type.OPERATOR, Operator.MINUS);
        checkNext(tokeniser, "max", Type.FUNCTION);
        checkNext(tokeniser, "(", Type.FUNCTION_LEFT_PAREN);
        checkNext(tokeniser, "-", Type.OPERATOR, Operator.UNARY_MINUS);
        checkNext(tokeniser, "1", Type.NUMBER, BigDecimal.ONE);
        checkNext(tokeniser, "-", Type.OPERATOR, Operator.MINUS);
        checkNext(tokeniser, "3", Type.NUMBER, new BigDecimal(3));
        checkNext(tokeniser, ",", Type.COMMA);
        checkNext(tokeniser, "-", Type.OPERATOR, Operator.UNARY_MINUS);
        checkNext(tokeniser, "4", Type.NUMBER, new BigDecimal(4));
        checkNext(tokeniser, ")", Type.FUNCTION_RIGHT_PAREN);
        checkNext(tokeniser, "-", Type.OPERATOR, Operator.MINUS);
        checkNext(tokeniser, "-", Type.OPERATOR, Operator.UNARY_MINUS);
        checkNext(tokeniser, "2", Type.NUMBER, new BigDecimal(2));
    }
    @Test
    public void tokeniseNested() throws Exception {
        Set<String> functionNames = new HashSet<>(Arrays.asList("max"));
        InfixTokeniser tokeniser = new InfixTokeniser("max(1, max((2+(3+4)), (5+6)))", functionNames, Collections.emptySet());
        checkNext(tokeniser, "max", Type.FUNCTION);
        checkNext(tokeniser, "(", Type.FUNCTION_LEFT_PAREN);
        checkNext(tokeniser, "1", Type.NUMBER, BigDecimal.ONE);
        checkNext(tokeniser, ",", Type.COMMA);
        checkNext(tokeniser, "max", Type.FUNCTION);
        checkNext(tokeniser, "(", Type.FUNCTION_LEFT_PAREN);
        checkNext(tokeniser, "(", Type.LEFT_PAREN);
        checkNext(tokeniser, "2", Type.NUMBER, new BigDecimal(2));
        checkNext(tokeniser, "+", Type.OPERATOR, Operator.PLUS);
        checkNext(tokeniser, "(", Type.LEFT_PAREN);
        checkNext(tokeniser, "3", Type.NUMBER, new BigDecimal(3));
        checkNext(tokeniser, "+", Type.OPERATOR, Operator.PLUS);
        checkNext(tokeniser, "4", Type.NUMBER, new BigDecimal(4));
        checkNext(tokeniser, ")", Type.RIGHT_PAREN);
        checkNext(tokeniser, ")", Type.RIGHT_PAREN);
        checkNext(tokeniser, ",", Type.COMMA);
        checkNext(tokeniser, "(", Type.LEFT_PAREN);
        checkNext(tokeniser, "5", Type.NUMBER, new BigDecimal(5));
        checkNext(tokeniser, "+", Type.OPERATOR, Operator.PLUS);
        checkNext(tokeniser, "6", Type.NUMBER, new BigDecimal(6));
        checkNext(tokeniser, ")", Type.RIGHT_PAREN);
        checkNext(tokeniser, ")", Type.FUNCTION_RIGHT_PAREN);
        checkNext(tokeniser, ")", Type.FUNCTION_RIGHT_PAREN);
    }

    private void checkNext(InfixTokeniser tokeniser, String expectedValue, Type expectedType, Operator expectedOperator) {
        OperatorToken next = (OperatorToken) tokeniser.next();
        assertEquals(expectedValue, next.getValue());
        assertEquals(expectedType, next.getType());
        assertEquals(expectedOperator, next.getOperator());
    }
    private void checkNext(InfixTokeniser tokeniser, String expectedValue, Type expectedType, BigDecimal expectedNumber) {
        NumericToken next = (NumericToken) tokeniser.next();
        assertEquals(expectedValue, next.getValue());
        assertEquals(expectedType, next.getType());
        assertEquals(expectedNumber, next.getNumber());
    }
    private void checkNext(InfixTokeniser tokeniser, String expectedValue, Type expectedType) {
        Token next = tokeniser.next();
        assertEquals(expectedValue, next.getValue());
        assertEquals(expectedType, next.getType());
    }

}