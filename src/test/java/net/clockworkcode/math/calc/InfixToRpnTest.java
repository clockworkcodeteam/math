package net.clockworkcode.math.calc;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * Created by david on 6/04/2016.
 */
public class InfixToRpnTest {
    // TODO add some bad input tests
    @Test
    public void convertComplexMultiplyBeforeDivide() throws Exception {
        convertAndCheck("3 + x * 0.2 / ( 1.89 - 5.89 ) ^ 2 ^ 3",
                        "3 x 0.2 * 1.89 5.89 - 2 3 ^ ^ / +");
    }

    @Test
    public void convertComplexDivideBeforeMultiply() throws Exception {
        convertAndCheck("3 + 4 / 0.2 * ( 1 - 5.89 ) ^ 2 ^ 3",
                        "3 4 0.2 / 1 5.89 - 2 3 ^ ^ * +");
    }

    @Test
    public void convertParenthesis() throws Exception {
        convertAndCheck("( 3 + 4 ) / ( 2 + 1 )",
                        "3 4 + 2 1 + /");
    }
    @Test
    public void convertNegativeNumbers() throws Exception {
        convertAndCheck("( -3 + 4 ) / ( 2 + -1.0 )",
                        "3 - 4 + 2 1.0 - + /");
    }

    @Test
    public void handleNoSpacesAroundParenthesis() throws Exception {
        convertAndCheck("(-3 - 4)/(2 + -1.0)",
                        "3 - 4 - 2 1.0 - + /");
    }

    @Test
    public void handleNoSpacesAroundOperators() throws Exception {
        convertAndCheck("3+4*0.2/(-1-5)^2^3",
                        "3 4 0.2 * 1 - 5 - 2 3 ^ ^ / +");
    }
    @Test
    public void handleNestedFunctions() throws Exception {
        convertAndCheck("max(1, max((2+(3+4)), (5+6)))",
                        "1 2 3 4 + + 5 6 + max max");
    }
    @Test
    public void handleNoSpacesVariations() throws Exception {
        String input = "3+4*2/(-1-5)^2^3";
        for(int i = 1; i < input.length()-1; i++) {
            convertAndCheck(input.substring(0, i) + " " + input.substring(i),
                    "3 4 2 * 1 - 5 - 2 3 ^ ^ / +");
        }
    }

    private void convertAndCheck(String input, String expected) {
        List<Token> strings = new InfixToRpn(new HashSet<>(Arrays.asList("max")), new HashSet<>(Arrays.asList("x"))).convert(input);
        String result = strings.stream().map(Token::getValue).collect(Collectors.joining(" "));
        assertEquals(expected, result);
    }
}